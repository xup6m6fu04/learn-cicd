@servers(['web' => ['root@inredis.com']])

@setup
    $repository = 'git@gitlab.com:xup6m6fu04/learn-cicd.git';
    $releases_dir = '/home/ubuntu/web/learn-cicd/releases';
    $app_dir = '/home/ubuntu/web/learn-cicd';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    # Optimise installation
    echo 'Optimising installation';
    cd {{ $app_dir }}/current;
    php artisan clear-compiled;
    php artisan optimize;
    php artisan config:cache;
    php artisan route:cache;
    php artisan cache:clear;
@endtask
